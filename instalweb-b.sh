#!/bin/bash
sudo apt -y update
sudo apt -y install git
sudo apt -y install docker.io 
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
chkonfig docker on

echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDSTKEKZRROyUGay7qYA1Uj1M77iPHhZ8PzLXmWw9EhAyg4pLequicjer5dhFqvSBC99fQnr6DwO0ZCHuDqPE/BY7kFui94wDgIHbrN0QJ6MOY76fc3quTpUjVa2ssYC1PKltpCbc31YWGesvPR/sh0UtYRqUp6b8cJT26MMVaNi4JFx9FtptIV6cTXMu69eedUhl+tirXQAOA+FhdDHdm/8Yz1XzsZOzufSuxRRM26TpZZKctMKuwt6LKKwwO30w+4kORDlyjeI77VTnuAkkKossWaBLH3Xh+POcPWPsgilhgd3w6GATmFtidDN/Ci5Jes2PQj2YT4zQAi4fKIZ3lbbO15lTXt0JJl6sj6QbcOWrsqSNStTPInuvD5s9RfMkoGj6/ljp5Qurk1egXIXnCKvsmgjfqebG8+k0xV77pLRe5OXmsGrS/3cj0ksrG4N76i01xFeFA1QyNv0kRF9SiQv37UFKmaupXOz0GrSNYEcq8bYrKA1e8Pn+/IEBPpYw0= loki@MiWiFi-R4CM-srv" >> /home/ubuntu/.ssh/authorized_keys
chown ubuntu:ubuntu -R /home/ubuntu/.ssh
chmod 700 /home/ubuntu/.ssh
chmod 600 /home/ubuntu/.ssh/authorized_keys

cd /home/ubuntu/
git clone -b master https://gitlab.com/andiydmitriv1/django.git

cd /home/ubuntu/django


sudo apt install -y apt install python3-pip
sudo pip install -r requirements.txt 

docker-compose up -d

sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64-fips"
sudo chmod +x /usr/local/bin/gitlab-runner
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
sudo -y apt  install awscli
sudo echo "gitlab-runner   ALL=(ALL)       NOPASSWD: ALL " >> /etc/sudoers


    
    
sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "GR1348941arUSzeQJSt3bzg9R1pWD" \
  --description "web-2" \
  --maintenance-note "web-2" \
  --tag-list "web-2" \
  --run-untagged="true" \
  --executor "shell"
 
sudo gitlab-runner restart
    
sudo apt install -y acl
sudo setfacl -R -m user:gitlab-runner:rwx /var/run/docker.sock
sudo setfacl -R -m user:gitlab-runner:rwx /home/*
sudo setfacl -dR -m user:gitlab-runner:rwx /home/*
sudo chown gitlab-runner:gitlab-runner -R /home/ubuntu/django
sudo rm /home/gitlab-runner/.bash_logout







