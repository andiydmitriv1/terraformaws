provider "aws" {
  access_key = $AWS_ACCESS_KEY_ID
  secret_key = $AWS_ACCESS_KEY_SECRET
  region     = $AWS_REGION
}




#================================= Create VPC ========================================



variable "vpc_cidr" {
    default = "10.0.0.0/16"
}


resource "aws_vpc" "my_test_task" {

        cidr_block = var.vpc_cidr
        tags = {
            Name = "My Test Task"
        }
  
}


resource "aws_internet_gateway" "my_test_task" {

    vpc_id = aws_vpc.my_test_task.id
  
}



resource "aws_subnet" "my_test_task_public" {
 
    cidr_block               =   "10.0.1.0/24"
    vpc_id                   =    aws_vpc.my_test_task.id
    map_public_ip_on_launch  =    true

    tags = {
      Name = "My Test Task Public Subnet"
    }
  
}


resource "aws_subnet" "my_test_task_private" {
 
    cidr_block               =   "10.0.2.0/24"
    vpc_id                   =    aws_vpc.my_test_task.id

    tags = {
      Name = "My Test Task Private Subnet"
    }
  
}




resource "aws_route_table" "route_table_public" {
  vpc_id = aws_vpc.my_test_task.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my_test_task.id
  }


  tags = {
    Name = "route table public test "
  }
}



resource "aws_route_table" "route_table_private" {
  vpc_id = aws_vpc.my_test_task.id
 

  route {
    cidr_block     = "0.0.0.0/0"
 #   gateway_id = aws_internet_gateway.my_test_task.id
    nat_gateway_id = aws_nat_gateway.nat_test.id
  }
  tags = {
    Name = "route table private test "
  }
}

resource "aws_eip" "elas_ip" {
  vpc      = true
}

resource "aws_nat_gateway" "nat_test" {
  allocation_id = aws_eip.elas_ip.id
  subnet_id     = aws_subnet.my_test_task_public.id

  tags = {
    Name = "my NAT"
  }
  depends_on = [aws_internet_gateway.my_test_task]
}




resource "aws_route_table_association" "my_test_task_public" {
  subnet_id      = aws_subnet.my_test_task_public.id
  route_table_id = aws_route_table.route_table_public.id
}

resource "aws_route_table_association" "my_test_task_private" {
  subnet_id      = aws_subnet.my_test_task_private.id
  route_table_id = aws_route_table.route_table_private.id
}






#================================= Security Group ========================================


resource "aws_security_group" "my_test_task_web" {

  vpc_id = aws_vpc.my_test_task.id

  ingress {

    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]

  }


  ingress {

    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]

  }

  ingress {

    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]

  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]

  }

  tags = {
    Name = "allow_tls"
  }
}





resource "aws_security_group" "my_test_task_vpn" {

  vpc_id = aws_vpc.my_test_task.id

  ingress {

    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]

  }


  ingress {

    from_port        = 1194
    to_port          = 1194
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]

  }

    ingress {

    from_port        = 943
    to_port          = 943
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]

  }

  ingress {

    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]

  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]

  }

  tags = {
    Name = "allow_tls"
  }
}




#================================= EC2 ========================================



resource "aws_instance" "web_server_a" {
    
    ami                     =   "ami-0f540e9f488cfa27d" # Ubuntu 22.0
    instance_type           =   "t3.micro"
    vpc_security_group_ids  =   [aws_security_group.my_test_task_web.id]
    subnet_id               =   aws_subnet.my_test_task_private.id
    user_data               =   "${file("instalweb-a.sh")}"


}


resource "aws_instance" "web_server_b" {
    
    ami                     =   "ami-0f540e9f488cfa27d" # Ubuntu 22.0
    instance_type           =   "t3.micro"
    vpc_security_group_ids  =   [aws_security_group.my_test_task_web.id]
    subnet_id               =   aws_subnet.my_test_task_private.id
    user_data               =   "${file("instalweb-b.sh")}"


}


resource "aws_instance" "open_vpn_server" {
    
    ami                     =   "ami-0f540e9f488cfa27d" # Ubuntu 22.0
    instance_type           =   "t3.micro"
    vpc_security_group_ids  =   [aws_security_group.my_test_task_vpn.id]
    subnet_id               =   aws_subnet.my_test_task_public.id
    user_data               =   "${file("instalvpn.sh")}"


}


resource "aws_eip" "vpn" {
  instance = aws_instance.open_vpn_server.id
  vpc      = true
}





